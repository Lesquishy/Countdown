data = {
"dates":[
  {"title":"Tauranga", "date":"2017, 11, 24, 19, 0, 0"},
  {"title":"Christmas", "date":"2017, 12, 25, 0, 0, 0"},
],
//Custom fontus must be defined in the Css
"fonts":[
  {"name":"Geonms","css":"Geonms"},
  {"name":"Arial","css":"Arial"},
  {"name":"Georgia","css":"Georgia"},
  {"name":"Tahoma","css":"Tahoma"},
  {"name":"Verdana","css":"Verdana"}
]
}
