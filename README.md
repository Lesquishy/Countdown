[![Status](https://img.shields.io/badge/Development-Stopped-red.svg?style=flat-square)]()

# Countdown
For update 0.95

It... Counts down!

It also counts up!

#What it currently does
 - It counts the difference between the current time and the set time. So it can count down and count up.

 - It counts in normal; Years, Months, Days, Hours, Minutes and Seconds. But also in; Total Days, Total Hours, Total Minutes and Total Seconds

 - It counts the distance between 2 locations (Based on people)

 - It can have different themes for different countdowns

 - Timer & Background Pausing

 - Customize what data is displayed

 - Invert text colors

 - Background Changes automatically

 - Change Frequency of background changes

 - Scroll though the different backgrounds

 - Easy to change dates and title

 - Customizable text from size to font

#Planned to Come
I'm fresh out of ideas. I'll Probably add a few more things then stop at version 1
