  <!-- Rugby Beentjes -->
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Countdown</title>
  <script src="https://code.jquery.com/jquery-1.12.0.js"></script>
  <link rel="stylesheet" type="text/css" href="./style.css">
  <link href='https://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
  <script src="./js/smol.js"></script>
  <script src="./js/timer.js"></script>
  <script src="./js/control.js"></script>
  <script src="./js/location.js"></script>
  <script src="./settings.js"></script>
</head>
<body>
  <?php
    $ip = $_SERVER['REMOTE_ADDR'];
    $myfile = fopen("ip", "a") or die("Unable to open file!");
    fwrite($myfile, $ip . ', ');
    fclose($myfile);
  ?>
  <div id="page">
    <div id="alert">
      <div id="display" onclick="hideAlert()">
        Hello
      </div>
    </div>
    <div onclick="settingsDecide()"><image src="./svg/setting.svg" id="expand"></image></div>
    <div id="allSettings">
      <div class="title"></div>
      <span id="song">Settings</span>
      <div id="media">
        <br>
        <table id="set">
          <tr><td class="setHead">General</td></tr>
          <tr>
            <td>Background Change: </td>
            <td>
              <div class="dropdown">
                <button onclick="myFunction()" class="dropbtn">Duration</button>
                <div id="myDropdown" class="dropdown-content">
                  <div id="first" onclick="timeing('first')">30s</div>
                  <div id="second" onclick="timeing('second')">1m</div>
                  <div id="third" onclick="timeing('third')">5m</div>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>Background Select: </td>
            <td>
              <div class="dropdown" id="backgroundDrop">
                <button onclick="backgroundDrop()" class="dropbtn">Selector</button>
                <div id="dropBackground" class="dropdown-content">
                  <div onclick="background(0)">Planet</div>
                  <div onclick="background(1)">Island</div>
                  <div onclick="background(2)">Hill</div>
                  <div onclick="background(3)">Sea</div>
                  <div onclick="background(4)">Sky</div>
                  <div onclick="background(5)">Snow</div>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>Background Pause: </td>
            <td>
            <div class="dropdown">
              <button onclick="pause('background')" class="dropbtn">Toggle</button>
            </div>
            </td>
          </tr>
          <tr><td class="spacer"></td></tr>
          <tr>
            <td>Font Select: </td>
            <td>
              <div class="dropdown" id="font">
                <button onclick="fontDrop()" class="dropbtn">Select</button>
                <div id="fontDrop" class="dropdown-content">
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>Font Size: </td>
            <td>
              <div class="dropdown" id="size">
                <button onclick="sizeDrop()" class="dropbtn">Select</button>
                <div id="sizeDrop" class="dropdown-content">
                  <div onclick="fontSize(4)">x0.5</div>
                  <div onclick="fontSize(3)">x0.7</div>
                  <div onclick="fontSize(0)">Default</div>
                  <div onclick="fontSize(1)">x1.3</div>
                  <div onclick="fontSize(2)">x1.5</div>
                </div>
              </div>
            </td>
          </tr>
          <tr><td class="spacer"></td></tr>
          <tr>
            <td>Timer Pause: </td>
            <td>
            <div class="dropdown">
              <button onclick="pause('time')" class="dropbtn">Toggle</button>
            </div>
            </td>
          </tr>
          <tr>
            <td>Invert: </td>
            <td>
            <div class="dropdown">
              <button onclick="invert()" class="dropbtn">Invert</button>
            </div>
            </td>
          </tr>
          <tr><td class="spacer"></td></tr>
          <tr>
            <td>
              Main Show/Hide:
            </td>
            <td>
              <div class="dropdown">
                <button onclick="main()" class="dropbtn">Toggle</button>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              Total Show/Hide:
            </td>
            <td>
              <div class="dropdown">
                <button onclick="total()" class="dropbtn">Toggle</button>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              Distance Show/Hide:
            </td>
            <td>
              <div class="dropdown">
                <button onclick="distanceToggle()" class="dropbtn">Toggle</button>
              </div>
            </td>
          </tr>
          <tr><td class="spacer"></td></tr>
          <tr>
            <td>
              Clear Saved Data
            </td>
            <td>
              <div class="dropdown">
                <button onclick="clearCookies(); alerts('Data Cleared', 'red')" class="bad dropbtn" id="clearData">Clear</button>
              </div>
            </td>

          </tr>
          <tr><td class="setHead">Preset Dates</td></tr>
        </table>
      </div>
    </div>
    <div id="loading">
      <span id="load">Loading... <br></span>
      <noscript>This site wont work without Javascript.</noscript>
    </div>
    <div id="content">
      <div id="title">Countdown</div>
      <br>
      <div class="wrap" id="main">
        <p id="years" class="largeNumber">00</p>
        <p class="large year mainText" >:</p>
        <p id="months" class="largeNumber">00</p>
        <p class="large month mainText">:</p>
        <p id="days" class="largeNumber">00</p>
        <br>
        <p class="texts" style="margin-top:-500px;"> <span id="year">YEARS : </span><span id="mont">MONTHS : </span> <span id="dayss">DAYS</span></p>
        <br>
        <p id="hours" class="width">00</p>
        <p class="colon">:</p>
        <p id="mins" class="width">00</p>
        <p class="colon">:</p>
        <p id="secs" class="width">00</p>
        <br>
        <p class="texts">HOURS : MINS : SEC</p>
      </div>
      <br>
      <div class="wrap" id="total">
        <p id="total" class="totalText">Total</p>
        <br>
        <p id="tdays" class="number">00</p>
        <p id="totalText" class="number"> DAYS </p><span class="pipe"> | </span>
        <p id="Thours" class="number">00</p>
        <p id="totalText" class="number"> HOURS </p><span class="pipe"> | </span>
        <p id="Tmins" class="number">00</p>
        <p id="totalText" class="number"> MINS </p><span class="pipe"> | </span>
        <p id="Tsecs" class="number">00</p>
        <p id="totalText" class="number"> SECS</p>
        <br>
      </div>
      <br>
      <div class="wrap" id="allDistance">
        <p id="distance">Distance: </p><p id="nDis"></p>
      </div>
      <a onclick="background('next')" id="next">Next Image</a>
    </div>
    <span id="version">Rugby Beentjes.<br> Update 0.95</span>
  </div>
</body>
</html>
<!-- Rugby Beentjes -->
