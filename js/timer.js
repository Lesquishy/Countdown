/*Rugby Beentjes*/
var done = false;
function time(year, month, day, hour, minutes, seconds) {
  $("#dayss, #days, #years, #year, .year, #months, .month, #mont").show();
  var then = new Date(year, month - 1, day, hour, minutes, seconds, 0);
  var now = new Date();
  var currentDate = now.getDate();
  var thenDate = then.getDate();

  var delta = Math.floor(Math.abs(then - now) / 1000);
  var delt = Math.floor(Math.abs(then - now));
  var Cminutes = 1000 * 60;
  var Chours = Cminutes * 60;
  var Cdays = Chours * 24;
  var Cmonths = Math.floor(Cdays * 30.5);
  var Cyears = Cdays * 365;


  //Calculation
  var years = Math.floor(delt / Cyears);
  delt -= years * Cyears;
  var months = Math.floor(delt / Cmonths % 12);
  delt -= Math.floor(months * Cmonths);
  var days = delt / Cdays % 31;
  delt -= Math.floor(days * Cdays);


  //Calculation
  var tdays = Math.floor(delta / 86400);
  delta -= tdays * 86400;
  var hours = Math.floor(delta / 3600) % 24;
  delta -= hours * 3600;
  var mins = Math.floor(delta / 60) % 60;
  delta -= mins * 60;
  var secs = Math.round(delta % 60);
  tsecs = parseInt(tdays) * 86400 + parseInt(hours) * 3600 + parseInt(mins) * 60 + secs;


  //Background Sorting.
  if(getCookie("backgroundPlay") == "true"){
    if(backgroundTime == 60000){
      if(secs == 59){
        background('next');
      }
    }else if (backgroundTime == 30000){
      if (secs == 30 || secs == 59){
        background('next');
      }
    } else if (backgroundTime == 300000){
      if ($("#hours").slice(-1) == "0" && secs == 30 || $("#hours").slice(-1) == "5" && secs == 30){
        background('next');
      }
    }
  }


  //Total Time
  thour = parseInt(tdays) * 24 + parseInt(hours);
  tmins = parseInt(hours) * 60 + 1440 * parseInt(tdays) + mins;


  //Making all the numbers positive
  mins = Math.abs(mins);
  hours = Math.abs(hours);
  secs = Math.abs(secs);
  tdays = Math.abs(tdays);
  thour = Math.abs(thour);
  tmins = Math.abs(tmins);
  tsecs = Math.abs(tsecs);


  //Rounding
  years = Math.floor(years);
  months = Math.floor(months);
  days = Math.floor(days);


  //Adding a 0 infront of its less than 9
  if (tdays <= 9) {
    tdays = '0' + tdays;
  }
  if (hours <= 9) {
    hours = '0' + hours;
  }
  if (mins <= 9) {
    mins = '0' + mins;
  }
  if (secs <= 9) {
    secs = '0' + secs;
  }
  if (years <= 9) {
    years = '0' + years;
  }
  if (months <= 9) {
    months = '0' + months;
  }
  if (days <= 9) {
    days = '0' + days;
  }


  //Display
  if($("#years").text() != years){
    $('#years').text(years);
  }
  if($("#months").text() != months){
    $('#months').text(months);
  }
  if($("#days").text() != days){
    $('#days').text(days);
  }
  if (tdays != $('#tdays').html()) {
    $('#tdays').text(tdays);
  }
  if (hours != $('#hours').html()) {
    $('#hours').text(hours);
  }
  if (mins != $('#mins').html()) {
    $('#mins').text(mins);
  }
  if($("#Thours").text() != thour){
    $('#Thours').text(thour);
  }
  if($("#Tmins").text() != tmins){
    $('#Tmins').text(tmins);
  }
  $("#Tsecs").text(tsecs);
  $('#secs').text(secs);


  //Hide options if 0
  if($("#months ").text() == "00"){
    $("#months, .month, #mont").hide();
  }
  if($("#years").text() == "00"){
    $("#years, #year, .year").hide();
  }
  if($("#days").text() == "00"){
    $("#dayss, #days").hide();
  }

  //Triggering First Load
  if (done === false) {
    $("body").trigger("time");
    done = true;
  }
}
