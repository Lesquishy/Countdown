/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

function backgroundDrop(){
  document.getElementById("dropBackground").classList.toggle("show");
  var setWidth = $("#backgroundDrop").css("width");
  $("#dropBackground").css({"width" : setWidth})
}

function fontDrop(){
  document.getElementById("fontDrop").classList.toggle("show");
  var setWidth = $("#font").css("width");
  $("#fontDrop").css({"width" : setWidth})
}

function sizeDrop(){
  document.getElementById("sizeDrop").classList.toggle("show");
  var setWidth = $("#size").css("width");
  $("#sizeDrop").css({"width" : setWidth})
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

function fontSelect(number){
  if(!number) number = getCookie("font");
  if(!number) number = 0;
  if(number > data["fonts"].length) {
    console.error("Font Number Too High! Resetting");
    number = 0;
  };
  $("html").css({"font-family":data["fonts"][number]["css"]});
  setCookie("font", number, "5");
}

function fontDraw(){
  var show = "";
  for(a = 0; a < data["fonts"].length; a++){
    show = show + "<div onclick='fontSelect(\"" + a + "\")'>" + data["fonts"][a]["name"] + "</div>"
  }
  $("#fontDrop").html(show);
}

function settingsSize(){
  var out = $(document).height() - 65;
  $("#media").css({"height":out});
}

function fontSize(size){
  var large = "8";
  var texts = "2";
  var midTexts = "3";
  var normal = "4";
  var total = "6";
  var padding = "0";
  if(size == ""){
    size = 0;
  }
  if(size == 1){
    large = "9";
    texts = "3";
    midTexts = "4";
    normal = "5";
    total = "7";
    padding = "5";
  }
  if(size == 2){
    large = "10";
    texts = "4";
    midTexts = "5";
    normal = "6";
    total = "8";
    padding = "6";
  }
  if(size == 3){
    large = "7";
    texts = "2";
    midTexts = "2";
    normal = "3";
    total = "5";
    padding = "0";
  }
  if(size == 4){
    large = "6";
    texts = "1";
    midTexts = "1";
    normal = "2";
    total = "4";
    padding = "0";
  }
  $("#tite, .largeNumber").css({"font-size": large+"em"});
  $(".texts").css({"font-size": texts+"em", "margin":padding+"px"});
  $(".midTexts").css({"font-size": midTexts+"em"});
  $(".number, .pipe").css({"font-size": normal+"em"});
  $(".totalText").css({"font-size": total+"em"});
  setCookie("fontSize", size, "5");
  //$("#title, .largeNumber, .texts, .midTexts, .number").css({"font-size":"4em"});
}

function hideDistance(){
  var current = getCookie('current');
  if(current == 0){
    $("#allDistance").show();
    locationCheck();
  } else {
    $("#allDistance").hide();
    $(document).trigger('distance');
  }
}

function buttonDisplay(){
  for(a = 0; a < data["dates"].length; a++){
    timer = a;
    alert = "'Changing to " + data["dates"][a]["title"]+"'";
    $("#set").html($("#set").html()+'<tr><td>' +  data["dates"][a]["title"] + '</td><td><div class="dropdown"><button onclick="currentTimer(true, ' + timer + '); alerts('+ alert + ')" class="dropbtn">Select</button></div></td></tr>')
  }
}

function listCookies() {
    var theCookies = document.cookie.split(';');
    var aString = '';
    for (var i = 1 ; i <= theCookies.length; i++) {
        aString += i + ' ' + theCookies[i-1] + "\n";
    }
    return aString;
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function clearTimer() {
  $("#months, #days, #hours, #mins, #secs, #Tsecs, #Tmins, #Thours, #tdays").text("00");
}

function removeAd() {
  $("body").on("jsReady", function() {
    $("#page").show();
  });
  $("body").children("*").get(1).remove();
}

function debug(close){
  $(".debug").show();
  if(close == true){
    $(".debug").hide();
  }
}

function invert(color){
  var Istate = getCookie("invert");
  if(first == true){
    if(Istate == "true"){
      $("#content, #next, #version").css({"color" : "white", "text-shadow" : "1px 1px 10px black"});
      setCookie("invert", "true", "5");
    } else if(Istate == "" && color == "white"){
      $("#content, #next, #version").css({"color" : "white", "text-shadow" : "1px 1px 10px black"});
      setCookie("invert", "true", "5");
    } else {
      $("#content, #next, #version").css({"color" : "black", "text-shadow" : "1px 1px 10px white"});
      setCookie("invert", "false", "5");
    }
  } else {
    if(Istate == "true"){
      $("#content, #next, #version").css({"color" : "black", "text-shadow" : "1px 1px 10px white"});
      setCookie("invert", "false", "5");
    } else {
      $("#content, #next, #version").css({"color" : "white", "text-shadow" : "1px 1px 10px black"});
      setCookie("invert", "true", "5");
    }
    alerts("Text Color Inverted");
  }
}

function timeing(item){
    if(item == "first"){
      backgroundTime = 30000;
      alerts("Background will refresh every 30 seconds");
    } else if (item == "second") {
      backgroundTime = 60000;
      alerts("Background will refresh every 60 seconds");
    } else if (item == "third"){
      backgroundTime = 300000;
      alerts("Background will refresh every 5 minutes");
    } else if (item == "fourth"){
      backgroundTime == 1800000;
      alerts("Background will refresh every 30 minutes");
    } else {
      $("body").html("oops");
    }
}

function total(){
  if(first == true){
    var toggle = getCookie("totalDisplay");
    if(toggle == "hide"){
      $("#total").css({"display" : "none"});
    } else $("#total").css({"display" : "block"});
  } else {
    if($("#total").css("display") == "none"){
      $("#total").css({"display" : "block"});
      setCookie("totalDisplay", "show", "5");
      alerts("Total Time Shown");
    } else {
      $("#total").css({"display" : "none"});
      setCookie("totalDisplay", "hide", "5");
      alerts("Total Time Hidden");
    }
  }
}

function main(){
  if(first == true){
    var toggle = getCookie("mainDisplay");
    if(toggle == "hide"){
      $("#main").css({"display" : "none"});
    } else {
      $("#main").css({"display" : "block"});
    }
  } else {
    if($("#main").css("display") == "none"){
      $("#main").css({"display" : "block"});
      setCookie("mainDisplay", "show", "5");
      alerts("Main Timer Shown");
    } else {
      $("#main").css({"display" : "none"});
      setCookie("mainDisplay", "hide", "5");
      alerts("Main Timer Hidden");
    }
  }
}

function distanceToggle(){
  if(first == true){
    var toggle = getCookie("distanceDisplay");
    if(toggle == "hide"){
      $("#allDistance").css({"display" : "none"});
    } else {
      $("#allDistance").css({"display" : "block"});
    }
  } else {
    if($("#allDistance").css("display") == "none"){
      $("#allDistance").css({"display" : "block"});
      setCookie("distanceDisplay", "show", "5");
      alerts("Distance Shown");
    } else {
      $("#allDistance").css({"display" : "none"});
      setCookie("distanceDisplay", "hide", "5");
      alerts("Distance Hidden");
    }
  }
}

function pause(item) {
  if(item == "background"){
    if(getCookie("backgroundPlay") == "true"){
      setCookie("backgroundPlay", "false", "5");
      alerts("Background Paused");
    } else {
      setCookie("backgroundPlay", "true", "5");
      alerts("Background Resumed");
    }
  } else if (item == "time") {
    if(getCookie("timerState") == "true"){
      setCookie("timerState", "false", "5");
      alerts("Timer Paused");
      window.clearInterval(currentTime);
    } else {
      setCookie("timerState", "true", "5");
      currentTimer();
      alerts("Timer Resumed");
    }
  }
}

function alerts(text, color){
  if (color == "green"){
    color = "#1caf39";
  } else if(color == "red"){
    color = "#960307";
  } else {
    color = "#0066cc";
  }
  $("#alert").css({"display" : "block", "background-color" : color});
  $("#display").text(text);
  setTimeout(function(){hideAlert()}, 8000);
}

function hideAlert(){
  $("#alert").fadeOut(800);
  setTimeout(function(){$("#display").text("Hide");}, 900);
}

function clearCookies(){
  $("#loading").fadeIn(400);
  setCookie("counter","","0.01");
  setCookie("backgroundPlay","","0.01");
  setCookie("timerState","","0.01");
  setCookie("mainDisplay","","0.01");
  setCookie("totalDisplay","","0.01");
  setCookie("current","","0.01");
  setCookie("openSetting","","0.01");
  setCookie("invert","","0.01");
  setCookie("iam","","0.01");
  setCookie("font","0","0.001");
  setCookie("fontSize", "", "0.001");
  setCookie("distanceDisplay", "", "0.01");
  setCookie("backgroundPhoto", "", "0.01");
  setTimeout(function(){location.reload(true);}, 900);
}
