/*Rugby Beentjes*/
//Start up
$(document).ready(function() {
  first = true;
  timer = false;
  backgrounds = false;
  distancer = true;
  backgroundTime = 60000;
  currentTime = null;

  //Time Text Settings
  alerts("We Use Cookies. Dont like it? Tough", "green");
  settingsSize();
  invert("white");
  fontDraw();
  fontSelect();
  total();
  main();
  settingsDecide();
  currentTimer();
  //Removed due to not having a ssl certificate
  //hideDistance();
  $("#distance").hide();
  //distanceToggle();
  buttonDisplay();
  fontSize(getCookie("fontSize"));

  background();
  first = false;
})

function settingsDecide(){
  var selector = $("#allSettings");
  if(first == true){
    if(getCookie("openSetting") == "true"){
      selector.show();
    } else {
      selector.hide();
    }
  } else {
    if(getCookie("openSetting") == "true"){
      selector.hide();
      setCookie("openSetting", "false", "5");
    } else {
      selector.show();
      setCookie("openSetting", "true", "5");
    }
  }
}

function check(){
  var state = false;
  if(timer == true && backgrounds == true && distancer == true ) {
    $(document).trigger("load");
  }
}

$(document).on("time", function() {
  timer = true;
  check("time");
});

$(document).on("distance", function() {
  distancer = true;
  check("distance");
});

$(document).on("backgrounds", function() {
  backgrounds = true;
  check();
});

$(document).on("load", function() {
  $('#loading').fadeOut(600);
});

  function currentTimer(replace, newOption){
  clearTimer();
  window.clearInterval(currentTime);
  var date = [];
  var title =[];
  for(a = 0; a < data["dates"].length; a++){
    date.push(data["dates"][a]["date"]);
    title.push(data["dates"][a]["title"])
  }
  var option = 0;
  if (replace == true){
    option = newOption;
    setCookie("current", option, "5")
  } else {
    if( !getCookie("current") ){
      setCookie("current", option, "5");
    } else {
      option = getCookie("current");
    }
  }
  //hideDistance();
  background();
  $('#title').text(title[option]);
  currentTime = window.setInterval('time(' + date[option] + ')',1000);
}

function background(action) {
  var imageNo;
  $('#loading').fadeIn(400);
  var image = ["./background/planet.jpg", "./background/island.jpg", "./background/hill.jpg", "./background/sea.jpg", "./background/sky.jpg", "./background/snow.jpg"];
  if(getCookie("current") == 2){
    var image = ["./background/xmas/mistletoe.jpg", "./background/xmas/lotsoftrees.jpg", "./background/xmas/starTree.jpg"];
  }
  imageNo = Math.round(Math.random() * (image.length - 1));
  if(getCookie("backgroundPhoto")){
    imageNo = getCookie("backgroundPhoto");
     if(imageNo > image.length){
       alerts("Undefined Image Number. Reseting", "red");
       imageNo = 0;
     }
  }
  if(action == "next"){
    if(image.length <= parseInt(imageNo) + 1){
      imageNo = 0;
    } else {
      imageNo++
    }
  } else if (action >= 0){
    imageNo = action;
  }

  var src = $('html').css({
    'background': 'url(' + image[imageNo] + ')',
    'background-attachment': 'fixed',
    'background-position': 'top',
    'background-size': 'cover'
  });
  var img = new Image();
  img.onload = function() {
    $(document).trigger("backgrounds");
    setCookie("backgroundPhoto", imageNo, "5")
  }
  img.src = image[imageNo];
  if (img.complete) img.onload();
}

function removeAd() {
  $("body").on("jsReady", function() {
    $("#page").show();
  });
  $("body").children("*").get(1).remove();
}
