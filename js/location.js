function getDistance(value) {
  var lat1;
  var lon1;
  var lat2;
  var lon2;

  lat1 = value[0][0];
  lon1 = value[0][1];
  lat2 = value[1][0];
  lon2 = value[1][1];


  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var final = Math.floor(R * c); // Distance in km
  $(document).trigger("distance");
  $("#nDis").text(final+"km")
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

function locationCheck(){
  var person;
  if(getCookie("iam")){
    var str = getCookie("iam");
    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
    alerts("Welcome " + str);
    person = getCookie("iam");
  } else {
    person = prompt("Insert First Name.", "");
    
    person = person.toLowerCase();
    setCookie("iam", person, "10");
  }
  geoFindMe(person);
}

function distance(){
  var person = ["rugby", "esther"];
  var end = [];
  var mid = null;
  for(a = 0; a < person.length; a++){
    $.ajax({
      type: "POST",
      url: './location/'+person[a]+'.txt',
      dataType: "text",
      success: function(data) {
        mid = data.split("|");
        end.push(mid)
      }
    });
  }
  setTimeout(function(){getDistance(end)},1000);
}

function whoRU(latitude, longitude, person){
  if(person == "rugby" || person == "esther"){
    $.post("./location/write.php", { name: person, latitude: latitude, longitude : longitude } );
    setTimeout(function(){distance()},2000);
  }
}

function geoFindMe(person) {
  if (!navigator.geolocation){
    alerts("Geolocation is not supported by your browser", "red");
    $("#allDistance").css({"display" : "none"})
    $(document).trigger("distance");
    return;
  }

  function success(position) {
    var latitude  = position.coords.latitude;
    var longitude = position.coords.longitude;
    whoRU(latitude, longitude, person);
  };

  function error() {
    alerts("Unable to retrieve your location", "red");
    $("#allDistance").css({"display" : "none"})
    $(document).trigger("distance");
  };

  navigator.geolocation.getCurrentPosition(success, error);
}
